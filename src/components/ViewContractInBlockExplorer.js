
export default function ViewContractInBlockExplorer({ address }) {
    return <a href={`https://blockscout.moonriver.moonbeam.network/address/${address}/transactions`} target="blank" >
        <h3>{`View ${address}`}</h3>
    </a>
}